# binary-search-twisted
lizst = [6,7,1,2,3,4,5]

# my solution
# time complexity: O(log n)
# space complexity O(1)
def binary_search(arr, target):
    left = 0
    right = len(arr) - 1
    while left <= right:
        middle = left + (right - left) // 2
        if arr[middle] == target:
            return middle
        elif arr[middle] > target: 
            if arr[right] < arr[middle] and arr[right] >= target:
                 left = middle + 1               
            else:
                right = middle - 1
        elif arr[middle] < target:
            if arr[left] > arr[middle] and arr[left] <= target:
                right = middle - 1
            else:
                left = middle + 1
    return -1 


# website solution
def binary_search_efficient(arr, target):
    left = 0
    right = len(arr) - 1
    while left <= right:
        middle = left + (right - left) // 2
        if arr[middle] == target:
            return middle
        elif arr[left] <= arr[middle]:
            if target > arr[middle] or target < arr[left]:
                left = middle + 1
            else:
                right = middle - 1
        elif arr[left] > arr[middle]:
            if target < arr[middle] or target > arr[right]:
                right = middle - 1
            else:
                left = middle + 1
    return -1



print(binary_search(lizst, 5))
print(binary_search_efficient(lizst, 5))


# a version of the binary search after a couple days
def binary_search_2(arr, target):
    l = 0
    r = len(arr) - 1
    while l <= r:
        mid = l + (r - l) // 2
        if arr[mid] == target:
            return mid
        elif arr[mid] >= arr[l]:
            if arr[l] > target or arr[mid] < target:
                l = mid + 1
            else:
                r = mid - 1
        else:
            if arr[r] < target or arr[mid] > target:
                r = mid - 1
            else:
                l = mid + 1
    return -1

print(binary_search_2(lizst, 5))