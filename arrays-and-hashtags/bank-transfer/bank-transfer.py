a = [2,4,1,1,2]
b = ['B','A','A', 'B','A']
c = [12,43,2,55,67,84]
d = ['B','B', 'A', 'A', 'A', 'B']
e = [2,]
f = ['A']


def adjust_bank_balances(target_bal, origin_bal, amount):
  return [target_bal + amount, origin_bal - amount]


def adjust_initial_balance(initial_balance, curr_balance):
  return [
      initial_balance + abs(curr_balance),
      curr_balance + abs(curr_balance),
  ]


# a = amount
# b = bank
def find_initial_balances(a, b):
  a_bal, b_bal = 0, 0
  a_init, b_init = 0, 0
  for i in range(len(a)):
    if b[i] == "A":
      a_bal, b_bal = adjust_bank_balances(a_bal, b_bal, a[i])
      if b_bal < 0:
        b_init, b_bal = adjust_initial_balance(b_init, b_bal)
    else:
      b_bal, a_bal = adjust_bank_balances(b_bal, a_bal, a[i])
      if a_bal < 0:
        a_init, a_bal = adjust_initial_balance(a_init, a_bal)

  return [a_init, b_init]


case1 = find_initial_balances(a, b)
assert case1 == [2, 4]

case2 = find_initial_balances(c, d)
assert case2 == [55, 69]

case3 = find_initial_balances(e, f)
assert case3 == [0, 2]
