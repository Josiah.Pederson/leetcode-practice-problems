# Given an integer array nums, return true if any value appears at 
# least twice in the array, and return false if every element is distinct.

input = [1,2,3,1]

# my solution
# time complexity: O(n)
# space complexity: O(2n)
def containsDuplicate(nums):
    values = {}
    for num in nums:
        if num in values:
            return True
        values[num] = 0
    return False

print(containsDuplicate(input))


# better solution:
# time complexity: O(n)
# space complexity: O(n)
def containsDuplicatesEfficient(nums):
    values = set()
    for num in nums:
        if num in values:
            return True
        values.add(num)
    return False

print(containsDuplicatesEfficient(input))