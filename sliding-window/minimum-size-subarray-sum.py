target = 7; nums = [2,3,1,2,4,3]


def minSubArrayLen(target, nums):
    """
    :type target: int
    :type nums: List[int]
    :rtype: int
    """
    start = 0
    end = 0
    sum = 0
    minimum = float("inf")
    # slide window size (right) until greater than target. then increase left. (dleete left value and move right)
    while end < len(nums):
        sum += nums[end]
        while sum >= target + nums[start]:
            sum -= nums[start]
            start += 1
        if sum >= target:
            minimum = min(minimum, end - start + 1)
        end += 1

    return 0 if minimum == float("inf") else minimum


print(minSubArrayLen(target, nums))