# You are given an array prices where prices[i] is the price of a given stock on the ith day.
# You want to maximize your profit by choosing a single day to buy one stock and choosing a different day in the future to sell that stock.
# Return the maximum profit you can achieve from this transaction. If you cannot achieve any profit, return 0.

prices = [7,2,4,3,6,1,9]

# my solution
# time complexity: O(n)
# space complexity: O(1)
def max_profit(prices):
    min = prices[0]
    max_diff = 0
    current_diff = 0
    for i in range(len(prices)):
        price = prices[i]
        if price < min:
            min = price
            if current_diff > max_diff:
                max_diff = current_diff
                current_diff = 0
        elif price - min > current_diff:
            current_diff = price - min
    return max(current_diff, max_diff)

print(max_profit(prices))


# alternate solution from video:
def max_profit_alternate(prices):
    left, right = 0, 1
    max_profit = 0
    while right < len(prices):
        if prices[left] < prices[right]:
            max_profit = max(max_profit, prices[right] - prices[left])
        else:
            left = right
        right += 1
    return max_profit

print(max_profit_alternate(prices))