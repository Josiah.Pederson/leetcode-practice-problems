palindrome = "A man, a plan, a canal: Panama"

# -- my solution --
# time complexity; O(n)
# space complexity: O(1)
def isPalindrome(s):
    start = 0
    stop = len(s) - 1
    while start < stop:
        while not s[start].isalnum():
            if start >= stop:
                break
            start += 1
        while not s[stop].isalnum():
            if start >= stop:
                break
            stop -= 1
        if s[start].lower() != s[stop].lower():
            return False
        start += 1
        stop -= 1
    return True

print(isPalindrome(palindrome))



# alternate solution with helper function (from video solution)
def isAlphaNumeric(character):
    return (
        "a" <= character.lower() <= "z" or
        "0" <= character <= "9"
    )

def isPalindromeEfficient(string):
    left = 0
    right = len(string) - 1
    while left < right:
        while left < right and not isAlphaNumeric(string[left]):
            left += 1
        while left < right and not isAlphaNumeric(string[right]):
            right -= 1
        if string[left].lower() != string[right].lower():
            return False
        left, right = left + 1, right - 1
    return True

print(isPalindromeEfficient(palindrome))