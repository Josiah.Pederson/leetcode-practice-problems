parenthesis = "({{{{}}}))"

# my verbose solution
def check_valid(open, close):
    return (
        ord(open) + 2 == ord(close) or
        ord(open) + 1 == ord(close)
    )

def isValid(s):
    i = 0
    open = ["(", "{", "["]
    stack = ""
    while i <= len(s) - 1:
        if i >= len(s) - 1 and s[i] in open:
            return False
        elif s[i] not in open:
            if len(stack) == 0:
                return False
            if check_valid(stack[0], s[i]):
                stack = stack[1:]
                i += 1
            else:
                return False
        else:
            if s[i + 1] in open:
                stack = s[i] + stack
                i += 1
            else:
                if check_valid(s[i], s[i + 1]):
                    i += 2
                else:
                    return False
    return len(stack) == 0

print(isValid(parenthesis))


def is_valid_efficient(s):
    close_to_open = { ")": "(", "]": "[", "}": "{" }
    stack = []
    for i in s:
        if i in close_to_open:
            if stack and stack[-1] == close_to_open[i]:
                stack.pop()
            else:
                return False
        else:
            stack.append(i)
    return True if not stack else False


print(is_valid_efficient(parenthesis))
